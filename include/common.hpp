#ifndef _COMMON_H
#define _COMMON_H

// Small definitions, structs etc. which don't (atm) need their own clas

#include <iostream>
#include <array>
#include <signal.h>
#include <mutex>

namespace cliviz {

#define SAMPLES 512
#define SAMPLE_BYTES 2
#define CHANNELS_AMOUNT 2
// this is fine
#define SAMPLE_RATE 44100
// BUFFER_SIZE is in bytes
#define BUFFER_SIZE SAMPLES * SAMPLE_BYTES

// Data structure for passing audio data from Audio Interface -> DSP
class Audio_Sample_Storage {
public:
    Audio_Sample_Storage();
    ~Audio_Sample_Storage();

    std::array<int16_t, BUFFER_SIZE> audio_samples;
    void samples_to_file();
    // Templating could be useful here, if in future we deal with different ints, or doubles/floats
    // The sample array could be a user defined data type :-)
    void set_samples(const std::array<int16_t, BUFFER_SIZE> &new_samples);
    const std::array<int16_t, BUFFER_SIZE> get_samples();
private:
    std::mutex thread_lock;
};

// Not really being used, is this necessary? vvvvv
class Notifier {
public:
    Notifier();
    ~Notifier();

    void set_shutdown(const bool &quit_signal) { this->shutdown = quit_signal; }
private:
    bool shutdown = false;
};

} // cliviz

#endif