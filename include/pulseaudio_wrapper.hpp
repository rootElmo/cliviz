#ifndef _PULSE_WRAPPER_H
#define _PULSE_WRAPPER_H

/*
https://freedesktop.org/software/pulseaudio/doxygen/simple.html#conn_sec
:)
*/

#include <pulse/pulseaudio.h>
#include <pulse/error.h>
#include <algorithm>
#include <memory>
#include <vector>
#include <signal.h>
#include <string>
#include <fstream>
#include <array>
#include <tuple>
#include <fftw3.h>
#include <math.h>
#include "audio_interface.hpp"
#include "common.hpp"

namespace cliviz {

class PulseAudio_Wrapper : public IAudio_Interface {

public:
    PulseAudio_Wrapper() {}
    PulseAudio_Wrapper(std::shared_ptr<Audio_Sample_Storage> audio_samples,
                        std::shared_ptr<bool> quit_notifier);
    ~PulseAudio_Wrapper();

    bool init() override;
    bool release() override;

    void read_audio() override;

    // ER: separate from _pa_signal_handle_callback for now, if we want to do something else than just quit PA mainloop
    void quit_pa_mainloop();

    // Process audio data from PA stream.
    void process_audio_data(int16_t *audio_data_bytes, const size_t sizeof_audio_data);
    void write_samples();

    uint32_t get_stream_sample_rate();

    // S16LE audio data samples read from PA stream
    std::vector<int16_t> samples;

    // Sample buffer for storing audio data from PA stream
    // TODO; create another class for these in the future
    std::array<int16_t, BUFFER_SIZE> audio_buffer1;
    std::array<int16_t, BUFFER_SIZE> audio_buffer2;
    std::tuple<std::array<int16_t, BUFFER_SIZE>, std::array<int16_t, BUFFER_SIZE>> audio_sample_data;
    uint16_t audio_data_iterator = 0;
private:
    // ER: maybe the '_pa'-portion of the variable names is redundant, after all,
    // they are private variables in the PA wrapper...
    pa_mainloop *_pa_loop = nullptr;
    pa_mainloop_api *_pa_api = nullptr;
    pa_context *_pa_context = nullptr;
    pa_signal_event *_pa_signal = nullptr;
    pa_stream *_pa_stream = nullptr;
    pa_sample_spec _pa_sample_spec;
    pa_server_info _pa_server_info;

    std::shared_ptr<bool> quit_notifier = nullptr;

    int _pa_error;


    // ER: Is this a good/correct use of static in this context? Im not sure
    // Later ER: at least here https://gist.github.com/jasonwhite/1df6ee4b5039358701d2
    // someone is using the way I'm using when it comes to the CB functions
    // VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV
    // Callback functions for PA
    // Callback for context's changes
    static void _pa_context_change_callback(pa_context *context, void* userdata);
    // Server information query callback... i think, check the docs
    static void _pa_server_info_callback(pa_context *context, const pa_server_info *info, void* /*userdata*/);
    // PA signal handle callback
    static void _pa_signal_handle_callback(pa_mainloop_api *api, pa_signal_event *event, int signal, void* userdata);
    // Read and handle fragment from PA stream
    static void _pa_stream_read_callback(pa_stream *stream, const size_t /*nbytes*/, void* userdata);
    // Stream change callback
    static void _pa_stream_change_callback(pa_stream *stream, void* userdata);

    void _set_stream();
};

} // cliviz

#endif // _PULSE_WRAPPER_H