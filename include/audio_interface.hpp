#ifndef _AUDIO_INTERFACE_H
#define _AUDIO_INTERFACE_H

#include <common.hpp>
#include <memory>
namespace cliviz {

class IAudio_Interface {

public:
    IAudio_Interface() {}
    IAudio_Interface(std::shared_ptr<Audio_Sample_Storage> audio_samples) : _audio_samples_storage(audio_samples) {}

    virtual bool init() = 0;
    virtual bool release() = 0;
    
    virtual void read_audio() = 0;
protected:
    std::shared_ptr<Audio_Sample_Storage> _audio_samples_storage;
private:

};

} // cliviz

#endif // _AUDIO_INTERFACE_H