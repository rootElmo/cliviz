#ifndef _PIPEWIRE_H
#define _PIPEWIRE_H

#include <pipewire-0.3/pipewire/pipewire.h>
#include "audio_interface.hpp"

namespace cliviz {

class Pipewire_Wrapper : public IAudio_Interface {

public:
    Pipewire_Wrapper();
    ~Pipewire_Wrapper(); 

    bool init() override;
    bool release() override;

    void read_audio() override;
private:
    
};

} // cliviz

#endif