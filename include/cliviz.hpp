#ifndef _CLIVIZ_H
#define _CLIVIZ_H

#include <iostream>
#include <memory>
#include "audio_interface.hpp"
#include "window.hpp"
#include "pulseaudio_wrapper.hpp"
#include "common.hpp"
#include "csignal"

namespace cliviz {

class Cliviz {

public:
    Cliviz();
    ~Cliviz();

    void init();
    void deinit();

    void start();

    static void handle_sigint(int signal);
    void fill_fft_samples(double *audio_samples_array);
    
    // bool quit = false;
private:
    // TODO: figure out, if we need to have audio interface as a pointer?
    // As in, do we need any other objects to have access to the audio interface?
    std::shared_ptr<IAudio_Interface> audio_interface = nullptr;
    std::shared_ptr<Audio_Sample_Storage> audio_samples = nullptr;
    std::shared_ptr<bool> quit = std::make_shared<bool>(false);
    static Cliviz *instance;

    void run_audio_interface_thread();    
};

} // cliviz

#endif // _CLIVIZ_H