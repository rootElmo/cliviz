#ifndef _WINDOW_H
#define _WINDOW_H

#include <ncurses.h>
#include <chrono>

namespace cliviz {

class Window {
public:
    Window();
    ~Window();

    void poll(); // Rename later to suit functionality
private:
    WINDOW *_cliviz_window = nullptr;
    int _current_win_x, _current_win_y = 0;
};

} // cliviz

#endif