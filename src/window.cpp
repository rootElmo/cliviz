#include "window.hpp"

using namespace cliviz;

Window::Window() {
    initscr();
    this->_cliviz_window = newwin(0,0,0,0);
}

void Window::poll() {
    // losing my mind over these
    std::chrono::time_point<std::chrono::high_resolution_clock> start;
    std::chrono::time_point<std::chrono::high_resolution_clock> stop;

    char getch_return = 0;
    int refreshes = 0;
    int line_current_y = 0;
    while (getch_return != 'c') {
        this->_current_win_y = getmaxy(this->_cliviz_window);
        this->_current_win_x = getmaxx(this->_cliviz_window);

        stop = std::chrono::high_resolution_clock::now();
        auto frametime = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
        int fps = (int) (1.0 / (frametime.count() / 1e6));

        // crude fps limiter?
        if (fps >= 120) {
            continue;
        }

        start = std::chrono::high_resolution_clock::now();

        wclear(this->_cliviz_window);
        //printw("I have been refreshed %d times!", refreshes);
        wprintw(this->_cliviz_window, "Screen size: %d %d", this->_current_win_x, this->_current_win_y);
        wmove(this->_cliviz_window, 1, 0);
        wprintw(this->_cliviz_window ,"FPS: %d", fps);

        for (int i = 0; i <= this->_current_win_x - 1; i++) {
            if (line_current_y >= this->_current_win_y) {
                line_current_y = 0;
            }
            wmove(this->_cliviz_window, 2 + line_current_y, i);
            wprintw(this->_cliviz_window, "#");
        }

        wrefresh(this->_cliviz_window);

        timeout(0);
        getch_return = getch();

        line_current_y++;
        refreshes++;
    }
}

Window::~Window() {
    endwin();
}