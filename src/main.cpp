#include <iostream>
#include <memory>
#include <ncurses.h>
#include "audio_interface.hpp"
#include "pipewire_wrapper.hpp"
#include "cliviz.hpp"
#include "csignal"

int main() {
    // init screen and sets up screen
    // initscr();
    
    cliviz::Cliviz the_program;
    // print to screen
    // printw("Hello World");

    // refreshes the screen
    // refresh();

    // pause the screen output
    // getch();

    // deallocates memory and ends ncurses
    // endwin();
    return EXIT_SUCCESS;
}