#include "cliviz.hpp"
#include <thread>

using namespace cliviz;

double window[SAMPLES];
double window_sum = 0.0;
double audio_samples[SAMPLES];
uint32_t sample_rate = SAMPLE_RATE;
int bandwidth = sample_rate / SAMPLES;

Cliviz *Cliviz::instance = nullptr;

void Cliviz::fill_fft_samples(double *audio_samples_array) {
    // there are SAMPLES * 2, since we record stereo
    std::array<int16_t, BUFFER_SIZE> audio_samples = this->audio_samples->get_samples();
    for (int i = 0; i < SAMPLES * 2; i += 2) {
        int sample = (int) audio_samples.at(i);
        // int16_t -> uint16_t -> double
        sample += 32768;
        double factorial_sample = (double) sample / 65535.0;
        audio_samples_array[i] = factorial_sample;
    }
}

Cliviz::Cliviz() {
    // Generate Hanning window
    for (int i = 0; i < SAMPLES; i++) {
        window[i] = 0.5 * (1 - cos(2 * M_PI * i / (SAMPLES - 1)));
    }
    double audio_samples[SAMPLES];
    // Allocate memory for FFTW input and output
    fftw_complex *out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * SAMPLES);
    double *in = (double*) fftw_malloc(sizeof(double) * SAMPLES);
    fftw_plan plan = fftw_plan_dft_r2c_1d(SAMPLES, in, out, FFTW_ESTIMATE);

    instance = this;
    std::signal(SIGINT, Cliviz::handle_sigint);

    this->audio_samples = std::make_shared<Audio_Sample_Storage>();
    // Run the whole PulseAudio shebang on another thread :)
    std::thread audio_interface_thread([this]() {this->run_audio_interface_thread();});

    while (*this->quit == false) {
        // Block "exit". Do other work here
        fill_fft_samples(audio_samples);
        window_sum = 0.0;
        for (int i = 0; i < SAMPLES; i++) {
            in[i] = audio_samples[i] * window[i];
            window_sum += window[i];
        }
        fftw_execute(plan);
    }

    audio_interface_thread.join();

    double magnitude_db[SAMPLES];

    for (int i = 1; i < SAMPLES / 2; i++) {
        double magnitude = sqrt(out[i][0] * out[i][0] + out[i][1] * out[i][1]);

        magnitude *= 2.0;
        magnitude /= window_sum;

        magnitude_db[i] = 20 * log10(magnitude);
    }

    for (int i = 0; i < SAMPLES / 2 + 1; i++) {
        printf("Bin %d: %f dB, band: %d Hz\n", i, magnitude_db[i], bandwidth * i );
    }

    fftw_free(in);
    fftw_free(out);
    fftw_destroy_plan(plan);
}

Cliviz::~Cliviz() {
}


void Cliviz::handle_sigint(int signal) {
    if (instance) {
        std::cout << "SIGINT received, shutting down" << std::endl;
        *instance->quit = true;
    }
}

void Cliviz::run_audio_interface_thread() {
    if (this->audio_samples != nullptr && this->quit != nullptr) {
        this->audio_interface = std::make_shared<PulseAudio_Wrapper>(this->audio_samples, this->quit);
        this->audio_interface->read_audio();
    }
}
