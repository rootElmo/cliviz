#include <fstream>
#include "common.hpp"

using namespace cliviz;

void Audio_Sample_Storage::samples_to_file() {

    std::ofstream samples_out("../samples");
    std::cout << "writing samples to file in Audio_Sample_Storage" << std::endl;

    // lock - blocks | try_lock - returns if if mutex is not available
    // blocking a thread may not be desireable, so try_lock
    // could/should be used in the future??
    std::lock_guard<std::mutex> lock(this->thread_lock);
    // 'i += 2'; only write left channel samples to file
    for (int i = 0; i < BUFFER_SIZE; i += 2) {
        samples_out << this->audio_samples.at(i) << std::endl;
    }

    samples_out.close();
}

void Audio_Sample_Storage::set_samples(const std::array<int16_t, BUFFER_SIZE> &new_samples) {
    std::lock_guard<std::mutex> lock(this->thread_lock);
    this->audio_samples = new_samples;
}

const std::array<int16_t, BUFFER_SIZE> Audio_Sample_Storage::get_samples() {
    std::lock_guard<std::mutex> lock(this->thread_lock);
    return this->audio_samples;
}

Audio_Sample_Storage::Audio_Sample_Storage() {

}

Audio_Sample_Storage::~Audio_Sample_Storage() {
    this->samples_to_file();
}