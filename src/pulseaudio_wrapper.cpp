#include <iostream>
#include <fstream>
#include "pulseaudio_wrapper.hpp"

using namespace cliviz;

bool PulseAudio_Wrapper::init() {
    // TODO: wrap these in if statements to check
    // if their respective inits fail or not
    this->_pa_loop = pa_mainloop_new();
    this->_pa_api = pa_mainloop_get_api(this->_pa_loop);

    signal(SIGPIPE, SIG_IGN);

    this->_pa_context = pa_context_new(this->_pa_api, "cliviz");
    pa_context_set_state_callback(this->_pa_context, &_pa_context_change_callback, this /*userdata*/);

    if (pa_context_connect(this->_pa_context, nullptr, PA_CONTEXT_NOFLAGS, nullptr) < 0) {
        std::cerr << "PulseAudio connection failed" << std::endl;
        return false;
    }

    return true;
}

void PulseAudio_Wrapper::_set_stream() {
    //this->_pa_stream = pa_stream_new(this->_pa_context, this->_pa_server_info.default_sink_name + '.monitor', 
    //                                    &this->_pa_sample_spec, &this->_pa_server_info.channel_map);
}

void PulseAudio_Wrapper::_pa_stream_change_callback(pa_stream *stream, void* userdata) {
    const pa_stream_state stream_state = pa_stream_get_state(stream);
    PulseAudio_Wrapper *paw = static_cast<PulseAudio_Wrapper*>(userdata);

    // These are copied straight from the API docs
    switch (stream_state) {
    case PA_STREAM_UNCONNECTED:
        std::cout << "The stream is not yet connected to any sink or source." << std::endl; 
        break;
    case PA_STREAM_CREATING:
        std::cout << "The stream is being created." << std::endl; 
        break;
    case PA_STREAM_READY:
        std::cout << "The stream is established, you may pass audio data to it now." << std::endl;
        if (paw) {
            pa_stream_set_read_callback(paw->_pa_stream, &paw->_pa_stream_read_callback, userdata /*userdata*/);
        }
        break;
    case PA_STREAM_FAILED:
        std::cout << "An error occurred that made the stream invalid." << std::endl; 
        break;
    case PA_STREAM_TERMINATED:
        std::cout << "The stream has been terminated cleanly." << std::endl; 
        break;
    default:
        break;
    }
}

// Send the "full back-array" of samples to store thread-safely
// the samples can then be read from another thread etc.
void PulseAudio_Wrapper::write_samples() {
    this->_audio_samples_storage->set_samples(std::get<1>(this->audio_sample_data));
}

// ER: Reads a fragment from the stream. Fragment size is set when creating a stream    
// Reading might read a full fragment's worth of data or less, take this into account
void PulseAudio_Wrapper::_pa_stream_read_callback(pa_stream *stream, const size_t /*nbytes*/, void* userdata) {
    PulseAudio_Wrapper *paw = static_cast<PulseAudio_Wrapper*>(userdata);

    std::cout << "Attempting to read audio data from stream" << std::endl;

    // The program (for now) gets audio data in signed 16 bit values so this is fine (for now)
    int16_t *actual_data_bytes = nullptr;
    // Amount of data IN BYTES!!!
    size_t sizeof_data = 0;
    // Peek a fragment of audio data from the recording stream
    if (pa_stream_peek(stream, (const void**)&actual_data_bytes, &sizeof_data) != 0) {
        std::cout << "Failed to peek at stream data" << std::endl;
        return;
    }

    // No data in the stream buffer
    if (actual_data_bytes == nullptr && sizeof_data == 0) {
        std::cout << "No data in stream buffer" << std::endl;
        return;
    // Hole in the stream buffer, there is no data, but a hole that is the size of sizeof_data exists
    } else if (actual_data_bytes == nullptr && sizeof_data > 0) {
        std::cout << "Hole in the stream buffer, dropping the hole" << std::endl;
        // Attempt to drop the hole from stream
        if (pa_stream_drop(stream) != 0) {
            std::cout << "Error on dropping hole from stream" << std::endl;
            return;
        }
    }
    
    // Process data
    if (paw) {
        int frames = sizeof_data / sizeof(int16_t);
        for (int i = 0; i < frames; i++) {
            paw->samples.push_back(actual_data_bytes[i]);

            if (paw->audio_data_iterator >= BUFFER_SIZE) {
                paw->audio_data_iterator = 0;
                // If the "front buffer" is full, swap the arrays, and do the FFT magic on the "back array"
                std::swap(std::get<0>(paw->audio_sample_data), std::get<1>(paw->audio_sample_data));
                std::get<0>(paw->audio_sample_data).at(paw->audio_data_iterator) = actual_data_bytes[i];

                // FFTW on std::get<1>(paw->audio_sample_data)
            } else {
                std::get<0>(paw->audio_sample_data).at(paw->audio_data_iterator) = actual_data_bytes[i];
            }

            paw->audio_data_iterator++;
        }
    }

    // Remove data from the buffer
    if (pa_stream_drop(stream) != 0) {
        std::cout << "Failed to drop data from record stream after peek" << std::endl;
    }
}

void PulseAudio_Wrapper::_pa_server_info_callback(pa_context *context, const pa_server_info *info, void* userdata) {
    PulseAudio_Wrapper *paw = static_cast<PulseAudio_Wrapper*>(userdata);
    if (paw) {
        paw->_pa_server_info = *info;

        // Channels need to match the running server's channels?
        paw->_pa_sample_spec.channels = CHANNELS_AMOUNT;
        // ER: If getting high frequencies (>~12kHz or so) isn't necessary the sample rate could be lower
        // this is usually around 44,1kHz or 48kHz
        //paw->_pa_sample_spec.rate = paw->_pa_server_info.sample_spec.rate;
        paw->_pa_sample_spec.rate = SAMPLE_RATE;
        paw->_pa_sample_spec.format = PA_SAMPLE_S16LE;

        // ER: Should this be moved to another function?
        // it's fine for now, 
        std::string stream_name = paw->_pa_server_info.default_sink_name;
        stream_name.append(".monitor");
        // TODO: ER: some error checking here
        // also: can the old stream be overwritten by pa_stream_new?
        paw->_pa_stream = pa_stream_new(context, stream_name.c_str() , &paw->_pa_sample_spec, &paw->_pa_server_info.channel_map);
        pa_stream_set_state_callback(paw->_pa_stream, &paw->_pa_stream_change_callback, userdata);
        
        pa_buffer_attr record_buffer_attributes;
        record_buffer_attributes.maxlength = (uint32_t) -1;
        // Read fragment size in BYTES
        // number of samples * size of sample
        // 512 * (2 bytes l-channel + 2 bytes r-channel)
        record_buffer_attributes.fragsize = (uint32_t) SAMPLES * SAMPLE_BYTES * CHANNELS_AMOUNT; 

        // STICK THE BUFFER ATTRIBUTES TO pa_stream_new. NO NEED TO DO IT DURING RUNTIME (for now???e)
        if (pa_stream_connect_record(paw->_pa_stream, stream_name.c_str(), &record_buffer_attributes, PA_STREAM_NOFLAGS) != 0) {
            std::cout << "Stream connection failed!!!!" << std::endl; // Take the pa_error here and print :)
        } else {
            std::cout << "Connected to " << stream_name << std::endl;
        }

    } else {
        std::cout << "Could not set server info or sample spec" << std::endl;
    }
    
    std::cout << "Default sink: " << info->default_sink_name << std::endl;
    std::cout << "Number of channels: " << info->sample_spec.channels << std::endl;
    std::cout << "Samplerate: " << info->sample_spec.rate << "Hz" << std::endl;
    std::cout << "Sample format: " << info->sample_spec.format << std::endl;

}

void PulseAudio_Wrapper::_pa_context_change_callback(pa_context *context, void* userdata) {
    const pa_context_state context_state = pa_context_get_state(context);
    switch (context_state) {
    case PA_CONTEXT_READY:
        std::cout << "PulseAudio context ready" << std::endl;
        pa_operation *op;
        op = pa_context_get_server_info(context, &_pa_server_info_callback, userdata /*userdata*/);
        // Unref the operation, causes memoryleak otherwise
        if (op) {
            pa_operation_unref(op);
        }
        break;
    case PA_CONTEXT_UNCONNECTED:
        std::cout << "PA context unconnected" << std::endl;
        break;
    case PA_CONTEXT_CONNECTING:
        std::cout << "PA context connecting..." << std::endl;
        break;
    case PA_CONTEXT_FAILED:
        std::cout << "PA context failed to connect, or was disconnected unexpectedly" << std::endl;
        break;
    case PA_CONTEXT_TERMINATED:
        std::cout << "PA context was terminated cleanly" << std::endl;
        break;
    case PA_CONTEXT_SETTING_NAME:
        // Check from the docs if this is right??
        std::cout << "PA context is given a name" << std::endl;
        break;
    case PA_CONTEXT_AUTHORIZING:
        std::cout << "PA context authorizing to daemon" << std::endl;
        break;
    default:
        std::cout << "PulseAudio context state: " << context_state << std::endl;
        break;
    }
}

// https://gist.github.com/jasonwhite/1df6ee4b5039358701d2
// This isn't being used, remove if 100% not needed
void PulseAudio_Wrapper::_pa_signal_handle_callback(pa_mainloop_api *api, pa_signal_event *event, int signal, void* userdata) {
    PulseAudio_Wrapper *paw = static_cast<PulseAudio_Wrapper*>(userdata);
    if (paw) {
        paw->quit_pa_mainloop();
    }
}

void PulseAudio_Wrapper::quit_pa_mainloop() {
    this->_pa_api->quit(this->_pa_api, 0);
}

void PulseAudio_Wrapper::read_audio() {
    while (*this->quit_notifier == false) {
        pa_mainloop_iterate(this->_pa_loop, 0, nullptr);
        this->write_samples();
    }
    this->quit_pa_mainloop();   
}

bool PulseAudio_Wrapper::release() {
    // stream
    pa_stream_disconnect(this->_pa_stream);
    pa_stream_unref(this->_pa_stream);
    // context
    pa_context_disconnect(this->_pa_context);
    pa_context_unref(this->_pa_context);

    // mainloop
    pa_mainloop_free(this->_pa_loop);
    std::cout << "Freeing PA and quitting" << std::endl;

    this->_pa_loop = nullptr;
    this->_pa_api = nullptr;
    this->_pa_context = nullptr;
    this->_pa_signal = nullptr;
    this->_pa_stream = nullptr;
    
    // always return true :--D
    return true;
}

uint32_t PulseAudio_Wrapper::get_stream_sample_rate() {
    return this->_pa_sample_spec.rate;
}

PulseAudio_Wrapper::PulseAudio_Wrapper(std::shared_ptr<Audio_Sample_Storage> audio_samples,
                                        std::shared_ptr<bool> quit_notifier) {

    this->_audio_samples_storage = audio_samples;
    this->quit_notifier = quit_notifier;
    this->init();
}

PulseAudio_Wrapper::~PulseAudio_Wrapper() {
    this->release();
}