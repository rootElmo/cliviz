#include <iostream>
#include "pipewire_wrapper.hpp"

using namespace cliviz;

bool Pipewire_Wrapper::init() {
    std::cout << "Initializing pipewire" << std::endl;
    pw_init(NULL, NULL);
    std::cout << "Compiled with libpipewire " << pw_get_headers_version() << std::endl;
    std::cout << "Linked with libpipewire " << pw_get_library_version() << std::endl;

    return true;
}

void Pipewire_Wrapper::read_audio() {
    
}

bool Pipewire_Wrapper::release() {
    pw_deinit();
    std::cout << "pipewire deinitialized" << std::endl;

    return true;
}

Pipewire_Wrapper::Pipewire_Wrapper() {
    this->init();
};
Pipewire_Wrapper::~Pipewire_Wrapper() {
    this->release();
}; 