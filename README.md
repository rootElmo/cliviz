# Cliviz

Cliviz (Command Line Interface VIZualizer) will be a audio visualizer for commandline interfaces, using **ncurses**.

# Table of contents

- [Description](#description)
- [Installation](#installation)
- [Usage](#usage)
- [Contributors](#contributors)
- [Material](#material)
- [License](#license)

# Description

The plan is to support **PipeWire**, and **Pulseaudio** and configure settings at runtime depending on system configurations.

Audio output of the system would be sampled, and the data visualized.

**FFTW** will be used for calculations. 

The initial plan is to have a simple **stereo VU meter**, and a simple **audio spectrum analyzer** with n number of frequencies.

# Installation

Instructions on installation & dependencies

Dependencies: **cmake**, **libpipewire-0.3-dev**, **libpulse-dev**, **ncurses**

```bash
$ sudo apt update && sudo apt install libpipewire-0.3-dev* libpulse-dev libncurses-dev cmake
```

**Building and running**:

```bash
$ mkdir build && cd build
$ cmake ..
$ make
$ ./cliviz
```

# Usage

Instructions on using the software

# Contributors

# Material

Material/literature related to the software, libraries used, design etc.

[Music-Visualizer - Audio visualizer using FFT algorithm and SFML](https://github.com/miha53cevic/Music-Visualizer). Great source of topics related to audio visualization, FFT etc.

[Pavucontrol sourcecode](https://github.com/pulseaudio/pavucontrol/tree/master).

[Displaying Audio Frequency Data in C++](https://www.youtube.com/watch?v=yt7i4zPbVDs)

[Audio samples to power spectrum](https://stackoverflow.com/questions/3058236/how-to-extract-frequency-information-from-samples-from-portaudio-using-fftw-in-c)

# License

Made by **[Elmo Rohula](https://gitlab.com/rootElmo)**
