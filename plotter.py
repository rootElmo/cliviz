# sudo apt update && sudo apt install pip
# pip install matplotlib numpy

import matplotlib.pyplot as plt
import numpy as np

data = []

with open('samples', 'r') as samples:
    data = [int(line.strip()) for line in samples.readlines()]


# print(data) # not necessary
plt.figure(1)
plt.title("Audio samples")
plt.plot(data)
plt.show()